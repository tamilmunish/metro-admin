import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { HttpModule } from '@angular/http';

declare var $: any;
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  address: any;
  balance: any;
  tokens = [];
  constructor(private auth: AuthService, private route: Router, private http: HttpModule) { 
    
  }

  ngOnInit() {
  }
  logOut() {
    this.auth.logout();
    this.route.navigate(['']);
  }

}
