import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Route, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.css']
})
export class HeaderBarComponent implements OnInit {

  constructor(private auth: AuthService, private route: Router, public toastr: ToastrService) { }

  ngOnInit() {
  }
  doLogout() {
    this.auth.logout()
    this.toastr.success('Successfully logged out');
    this.route.navigate(['login']);

  }
}
