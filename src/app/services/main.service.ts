import { Injectable } from '@angular/core';
import { ENV } from '../core/env.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
declare let require: any;
declare let window: any;
import * as xss from 'xss';
import { AuthService } from '../services/auth.service';


@Injectable()
export class MainService {

  
  constructor(private http: HttpClient, private auth: AuthService,) {
  }
  
 
  getOrder = function () {
    return this.http.get(ENV.GETORDER,{
      headers: new HttpHeaders().set('Authorization', this.auth.getAccessToken())
  });
  };
}
