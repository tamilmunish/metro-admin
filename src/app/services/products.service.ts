import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ENV } from '../core/env.config';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient, private auth: AuthService) { }
  getCategory = function () {
    return this.http.get(ENV.GETCATEGORY,{
      headers: new HttpHeaders().set('Authorization', this.auth.getAccessToken())
  });
  };
  getProduct = function () {
    return this.http.get(ENV.GETPRODUCT,{
      headers: new HttpHeaders().set('Authorization', this.auth.getAccessToken())
  });
  };
  addProduct = function (data) {
    return this.http.post(ENV.ADDPRODUCT, data ,{
      headers: new HttpHeaders().set('Authorization', this.auth.getAccessToken())
  });
  };
}
