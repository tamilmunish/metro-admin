import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ENV } from '../core/env.config';
import { HttpClientModule, HttpClient, HttpHeaders, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';
import { map } from  'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient, private auth: AuthService) { }

  getCategory = function () {
    return this.http.get(ENV.GETCATEGORY,{
      headers: new HttpHeaders().set('Authorization', this.auth.getAccessToken())
  });
  };
  addCategory = function (data) {
    return this.http.post(ENV.ADDCATEGORY, data ,{
      headers: new HttpHeaders().set('Authorization', this.auth.getAccessToken())
  });
  };
  public upload(data) {

    return this.http.post<any>(ENV.ADDSINGLEIMAGE, data, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    })
    );
  }
}
