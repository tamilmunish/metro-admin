const _isDev = window.location.port.indexOf('4200') > -1;

const getHost = () => {
    const protocol = window.location.protocol;
    const host = window.location.host;
    // return 'https://axir-network-rest-timely-klipspringer.mybluemix.net/api';
    return 'http://localhost:3000/api/v1/';
};

const TEST_NET = true;
const apiURI = getHost() + '';
console.log(apiURI);
export const ENV = {
    BASE_URI: getHost(),
    LOGIN: apiURI+"sellers/login",
    ADDPRODUCT: apiURI+"sellers/login",
    GETPRODUCT: apiURI+"sellers/login",
    ADDCATEGORY: apiURI+"categories",
    GETCATEGORY: apiURI+"categories",
    GETORDER: apiURI+"sellers/login",
    GETUSER: apiURI+"sellers/login",
    ADDSINGLEIMAGE: apiURI+"images/upload-single-image",
};
