import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AuthService } from './services/auth.service';
import { MainService } from './services/main.service';
import { ModalService } from './services/modal.service';
import { GuestGuard } from './guards/guest.guard';
import { AuthGuard } from './guards/auth.guard';
import { SideBarComponent } from './partials/side-bar/side-bar.component';
import { HeaderBarComponent } from './partials/header-bar/header-bar.component';
import { ModalComponent } from './directives/modal.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { TruncatePipe } from './pipe';
import { OrderBy } from './orderby';
import { AddproductComponent } from './components/addproduct/addproduct.component';
import { ProductlistComponent } from './components/productlist/productlist.component';
import { ProductsService } from './services/products.service';
import { CategoryComponent } from './components/category/category.component';
import { AddCategoryComponent } from './components/add-category/add-category.component';
import { CategoryService } from './services/category.service';


const routes: Routes = [
  // { path: '', redirectTo: '/login', pathMatch: 'full' ,},
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'add-product', component: AddproductComponent, canActivate: [AuthGuard] },
  { path: 'list-product', component: ProductlistComponent, canActivate: [AuthGuard] },
  { path: 'add-category', component: AddCategoryComponent, canActivate: [AuthGuard] },
  { path: 'list-category', component: CategoryComponent, canActivate: [AuthGuard] },

  { path: '**', redirectTo: '' }

  // { path: 'mobileoperator', component: MobileoperatorComponent },
  // {
  //   path: 'admin',
  //   children: [
  //     { path: 'contract', component: ContractComponent }],
  //   component: HomeComponent,
  //   canActivate: [AuthGuard]
  // }
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SideBarComponent,
    HeaderBarComponent,
    ModalComponent,
    TruncatePipe,
    OrderBy,
    AddproductComponent,
    ProductlistComponent,
    CategoryComponent,
    AddCategoryComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    AuthService,
    GuestGuard,
    AuthGuard,
    MainService,
    ModalService,
    ProductsService,
    CategoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
