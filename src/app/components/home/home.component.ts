import { Component, OnInit } from '@angular/core';

import { ENV } from '../../core/env.config';
import { $WebSocket } from 'angular2-websocket/angular2-websocket'
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthService } from '../../services/auth.service';
import { getTranslationForTemplate } from '@angular/core/src/render3/i18n';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MainService } from '../../services/main.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  buyForm: FormGroup;

  buy = [];
  sell =   [];
  constructor(private fb: FormBuilder, private main: MainService, private http: Http, private auth: AuthService,) {
   
  }
  get quantity() { return this.buyForm.get('email'); }
  ngOnInit() {
    this.getSell();
    this.getBuy();
    this.buyForm = new FormGroup({
      quantity: new FormControl('', [Validators.required]),
    });
  }
  getSell() {
    this.main.getSell().subscribe((res) => {
      this.sell = res.body;
    });
  }
  getBuy() {
    this.main.getBuy().subscribe((res) => {
      this.buy = res.body;
    });
  }
  addBuyTrade() {
    this.main.getBuyTrade(this.buyForm.value).subscribe((res) => {
      this.getSell();
    });
  }
 
}
