import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {

  productForm: FormGroup;
  constructor(private fb: FormBuilder,private http: Http,  public toastr: ToastrService) { }
  get sku() { return this.productForm.get('sku'); }
  get description() { return this.productForm.get('description'); }

  ngOnInit() {
  }

}
