import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Route, Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  // providers: [AuthService, FormGroup]
})
export class LoginComponent implements OnInit {
  userForm: FormGroup;
  constructor(private fb: FormBuilder,private http: Http, private auth: AuthService, private route: Router, public toastr: ToastrService) { }
  get email() { return this.userForm.get('email'); }
  get password() { return this.userForm.get('password'); }
  ngOnInit() {
    // console.log(this.auth.getUser());
    this.userForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }
  doLogin() {
    if (!this.userForm.valid) { return; }
    this.auth.login(this.userForm.value).subscribe((res) => {
      console.log(res)
      if (res.status === 'success') {
        this.auth.setUser(res.response);
        this.auth.setAccessToken(res.response.token);
        this.toastr.success('Successfully login');
        this.route.navigate(['']);
      }
      else {
        this.toastr.error('Error login!');

      }

    });
  }
}
