import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Http, Headers, RequestOptions } from '@angular/http';


@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  categoryForm: FormGroup;
  constructor(private fb: FormBuilder,private http: Http,  public toastr: ToastrService) { }
  get name() { return this.categoryForm.get('name'); }
  get description() { return this.categoryForm.get('description'); }

  ngOnInit() {
  }

}
